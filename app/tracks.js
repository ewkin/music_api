const express = require('express');
const Track = require('../models/Track');
const router = express.Router();


router.get('/', async (req, res) => {
    try {
        const criteria = {};
        let track = []
        if (req.query.album) {
            criteria.album = req.query.album;
        }
        if (req.query.artist) {
            const tracks = await Track.find().populate('album');
            tracks.map(oneTrack =>{
               if(oneTrack.album.artist==req.query.artist){
                   track.push(oneTrack);
               }
            });
        } else {
            track = await Track.find(criteria).populate('album', 'name');
        }
        if (track) {
            res.send(track);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', async (req, res) => {
    const trackData = req.body;
    const track = new Track(trackData);
    try {
        await track.save();
        res.send(track);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;