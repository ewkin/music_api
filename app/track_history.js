const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');

const router = express.Router();


router.post('/', async (req, res) => {
    const token = req.get('Authorization');
    if (!token) {
        return res.status(401).send({error: 'No token password'});
    }
    const user = await User.findOne({token});
    if (!user) {
        return res.status(401).send({error: 'Wrong token'});
    }

    const trackHistoryData = req.body;
    trackHistoryData.user = user._id;
    const trackHistory = new TrackHistory(trackHistoryData);
    try {
        await trackHistory.setDatetime();
        await trackHistory.save();
        res.send(trackHistory);
    } catch (e) {
        res.status(400).send(e);
    }
});

module.exports = router;