const path = require('path');
const express = require('express');
const multer = require('multer');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath)
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const criteria = {};
        if (req.query.artist) {
            criteria.artist = req.query.artist;
        }
        const albums = await Album.find(criteria).populate('artist', 'name');
        if (albums) {
            res.send(albums);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const albums = await Album.findOne({_id: req.params.id}).populate('artist', 'name');
        if (albums) {
            res.send(albums);
        } else {
            res.sendStatus(404);
        }
    } catch (e) {
        res.sendStatus(500);
    }
});


router.post('/', upload.single('image'), async (req, res) => {
    try {
        const albumData = req.body;

        if (req.file) {
            albumData.image = req.file.filename;
        }
        const album = new Album(albumData);
        await album.save();
        res.send({album});
    } catch (e) {
        res.status(400).send(e);
    }

});

module.exports = router;
